import setting from './api-settings'

export default {
    async FETCH_USERS(){
        const headers = setting.headers
        const res = await fetch(setting.url+"/user", { headers })
        const {data} = await res.json();
        return data
    },

    async FETCH_USER(user_id){
        const headers = setting.headers
        const res = await fetch(setting.url+"/user/" + user_id, { headers })
        const json = await res.json();
        return json
      }
}

