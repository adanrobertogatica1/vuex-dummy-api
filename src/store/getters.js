export default {
    users_list: state => state.users_list,
    user_profile: state => state.user_profile,
    fetching_data: state => state.fetching_data  
}