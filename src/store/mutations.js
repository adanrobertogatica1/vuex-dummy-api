import * as type from './mutations-types'

export default {
    //Fetch de los usuarios
    [type.FETCH_USERS_REQUEST](state){
        state.fetching_data = true
        state.error = null
    },

    [type.FETCH_USERS_SUCCESS](state,  data ){
        state.fetching_data = false
        state.error = null
        state.users_list = data
    },

    [type.FETCH_USERS_FAILURE](state, { error }){
        state.fetching_data = false
        state.error = error
    },

    //Fetch de un usuario en singular
    [type.FETCH_USER_REQUEST](state){
        state.fetching_data = true
        state.error = null
    },

    [type.FETCH_USER_SUCCESS](state, user_profile){
        state.fetching_data = false
        state.error = null
        state.user_profile = user_profile
    },

    [type.FETCH_USER_FAILURE](state, { error }){
        state.fetching_data = false
        state.error = error
    }
}