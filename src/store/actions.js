import * as type from './mutations-types'
import API  from '@/api'

export default {
    fetchUsers({commit}){
        commit(type.FETCH_USERS_REQUEST)
        API.FETCH_USERS().then(data => commit(type.FETCH_USERS_SUCCESS, data) )
        .catch(error => commit(type.FETCH_USERS_FAILURE, { error }))
    },
    fetchUser({commit},user_id){
        commit(type.FETCH_USER_REQUEST)
        API.FETCH_USER(user_id).then(json => commit(type.FETCH_USER_SUCCESS, json) )
        .catch(error => commit(type.FETCH_USER_FAILURE, { error }))
    }
}
